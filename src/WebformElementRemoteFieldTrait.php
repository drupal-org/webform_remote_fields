<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\Form\FormStateInterface;
use Drupal\multivalue_form_element\Element\MultiValue;
use Drupal\webform_remote_fields\Service\Handler;
use Symfony\Component\Yaml\Yaml;

/**
 * Helper Webform element became remote field.
 */
trait WebformElementRemoteFieldTrait {

  /**
   * Default properties for each remote field.
   *
   * @return array
   *   An array with mandatory properties for remote fields.
   */
  public function remoteFieldBaseProperties(): array {
    return [
      'api_endpoint' => '',
      'needs_header' => FALSE,
      'headers' => '',
      'api_endpoint_add_query_parameter' => FALSE,
      'api_result_select_value' => '',
      'api_endpoint_params' => [],
    ];
  }

  /**
   * Alter form adding mandatory elements.
   *
   * @param array $form
   *   The configuration element form.
   */
  public function baseFormFields(array &$form): void {
    $form['wr_field'] = [
      '#type' => 'details',
      '#title' => $this->t('API Settings'),
      '#weight' => -30,
      '#open' => TRUE,
    ];

    $form['wr_field']['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API endpoint'),
      '#description' => $this->t('The endpoint that will be called to retrieve the options. For internal endpoints use [internal]/your-api-end-point'),
      '#weight' => 1,
      '#required' => TRUE,
    ];

    $form['wr_field']['needs_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add custom headers'),
      '#weight' => 1,
    ];

    $form['wr_field']['headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Headers'),
      '#description' => $this->t('Add one header per line, like: Authorization: Basic token here'),
      '#weight' => 1,
      '#states' => [
        'visible' => [
          ':input[name="properties[needs_header]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['wr_field']['api_result_select_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API JSON Prop - Value'),
      '#description' => $this->t('The JSON prop to be used as the select value'),
      '#weight' => 3,
      '#required' => TRUE,
    ];

    $form['wr_field']['api_endpoint_add_query_parameter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add query parameters'),
      '#weight' => 5,
    ];

    $form['wr_field']['api_endpoint_params'] = [
      '#type' => 'multivalue',
      '#title' => $this->t('API Params'),
      '#cardinality' => MultiValue::CARDINALITY_UNLIMITED,
      '#weight' => 7,
      'param_name' => [
        '#type' => 'textfield',
        '#title' => $this->t('The API endpoint param name'),
      ],
      'param_value' => [
        '#type' => 'textfield',
        '#title' => $this->t('The API endpoint param value'),
        '#description' => $this->t('The webform element machine name to get the value and use as a param value'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="properties[api_endpoint_add_query_parameter]"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState) {
    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = \Drupal::routeMatch()->getParameter('webform');
    $handler = $webform->getHandlers();
    $handlersList = $handler->getInstanceIds();
    // As remote field needs Webform Remote Field Handler.
    // Make sure to set this handler when any remote field is saved.
    if (!in_array('webform_remote_fields_handler', array_keys($handlersList))) {
      /** @var \Drupal\webform\Plugin\WebformHandlerManagerInterface $handlerManager */
      $handlerManager = \Drupal::service('plugin.manager.webform.handler');
      $handler = $handlerManager->createInstance('webform_remote_fields_handler', [
        'id' => 'webform_remote_fields_handler',
        'label' => 'Webform Remote Fields Handler',
        'handler_id' => 'webform_remote_fields_handler',
        'status' => 1,
        'weight' => 0,
        'settings' => [],
      ]);
      $webform->addWebformHandler($handler);
      $webform->save();
    }
    parent::submitConfigurationForm($form, $formState);
    \Drupal::service('cache_tags.invalidator')
      ->invalidateTags([
        Handler::CACHE_TAG . ':' . $formState->getValue('api_endpoint'),
      ]);
  }

  /**
   * Validate remote form field settings.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $formState): void {
    parent::validateConfigurationForm($form, $formState);
    $currentWebform = $this->getWebform();
    $tokenService = \Drupal::service('token');
    // Validate endpoint params.
    $params = $formState->getValue('api_endpoint_params');
    if ($formState->getValue('api_endpoint_add_query_parameter') && empty($params)) {
      $formState->setErrorByName(
        'api_endpoint_params',
        t('Missing endpoint params, required if use other fields checked.')
      );
    }
    elseif (!empty($params)) {
      foreach ($params as $values) {
        $tokens = $tokenService->scan($values['param_value'] . $values['param_name']);
        if (!empty($tokens['form_value'])) {
          foreach (array_keys($tokens['form_value']) as $field) {
            $fieldName = str_replace('name:', '', $field);
            if (!$currentWebform->getElement($fieldName)) {
              $formState->setErrorByName(
                'api_endpoint_params',
                t('Field @field used as token does not exists.', ['@field' => $fieldName])
              );
            }
          }
        }
      }
    }

    // Validate headers.
    if ($formState->getValue('needs_header')) {
      $invalidHeaders = FALSE;
      try {
        $result = Yaml::parse($formState->getValue('headers'));
        if (!is_array($result)) {
          $invalidHeaders = TRUE;
        }
      }
      catch (\Exception $exception) {
        $invalidHeaders = TRUE;
        \Drupal::logger('webform_remote_fields')
          ->error($exception->getMessage());
      }
      if ($invalidHeaders) {
        $formState->setErrorByName('headers', t('Invalid headers.'));
      }
    }

    $settings = [
      'api_endpoint',
      'api_result_select_value',
      'api_result_select_text',
      'headers',
    ];
    foreach ($settings as $setting) {
      $tokens = $tokenService->scan($formState->getValue($setting));
      foreach ($tokens as $token) {
        foreach ($token as $fieldName => $value) {
          $fieldName = explode(':', $fieldName)[1];
          $exists = $currentWebform->getElement($fieldName);
          if (!$exists) {
            $formState->setErrorByName(
              $setting,
              t('Field @field used as token does not exists.', ['@field' => $fieldName])
            );
          }
        }
      }
    }
  }

}
