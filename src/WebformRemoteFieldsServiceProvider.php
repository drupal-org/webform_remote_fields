<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\webform_remote_fields\Service\WebformRemoteFieldTokenManager;

/**
 * Webform remote field service provider.
 */
class WebformRemoteFieldsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function register(ContainerBuilder $container) {
    $webformTokenManagerDefinition = $container->getDefinition('webform.token_manager');
    if (!empty($webformTokenManagerDefinition)) {
      $webformTokenManagerDefinition->setClass(WebformRemoteFieldTokenManager::class);
    }
  }

}
