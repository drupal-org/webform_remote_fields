<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_remote_fields\Event\WebformRemoteFieldAlterResponseEvent;
use Symfony\Component\Yaml\Yaml;

/**
 * Provide shared features between Webform Remote Field elements.
 */
trait RemoteFieldBaseTrait {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#process'][] = [
      static::class,
      'processLibrary',
    ];
    return $info;
  }

  /**
   * Attach library to trigger element change.
   */
  public static function processLibrary($element, FormStateInterface $form_state, $form) {
    $element['#attached']['library'][] = 'webform_remote_fields/webform_remote_fields';
    return $element;
  }

  /**
   * Make the requests for each remote field element.
   */
  protected static function processElementRequest($element, FormStateInterface $formState) {
    $tokenService = \Drupal::token();
    $result = [
      'error' => FALSE,
      'data' => NULL,
    ];
    $apiSettings = self::extractElementSettings($element, $formState);
    $endpoint = $apiSettings['api_endpoint'];
    if (empty($endpoint)) {
      return $result;
    }
    $requestOptions = [];

    // Mount headers.
    $headers = [];
    if ($apiSettings["needs_header"]) {
      $headerValues = explode("\n", $element['#headers']);
      foreach ($headerValues as $headerValue) {
        $tokens = $tokenService->scan($headerValue);
        if (empty($tokens['form_value'])) {
          [$headerKey, $headerKeyValue] = explode(':', $headerValue);
          $headers[trim($headerKey)] = trim($headerKeyValue);
          continue;
        }
        foreach ($tokens['form_value'] as $fieldName => $token) {
          $fieldName = explode(":", $fieldName)[1];
          $fieldValue = $formState->getValue($fieldName);
          if (empty($fieldValue)) {
            return $result;
          }
          $headerLineReplaced = $tokenService->replace($headerValue, ['form_value' => $formState->getValues()]);
          [$headerKey, $headerKeyValue] = explode(':', $headerLineReplaced);
          $headers[trim($headerKey)] = trim($headerKeyValue);
        }
      }
    }

    if (!empty($headers)) {
      $requestOptions['headers'] = $headers;
    }

    $queryParams = [];
    // Check query params.
    if (!empty($element['#api_endpoint_params'])) {
      foreach ($element['#api_endpoint_params'] as $param) {

        // Replace if need param_name.
        $paramName = $param["param_name"];
        $tokens = $tokenService->scan($param["param_name"]);
        if (!empty($tokens['form_value'])) {
          foreach ($tokens['form_value'] as $fieldName => $token) {
            $fieldName = explode(":", $fieldName)[1];
            $fieldValue = $formState->getValue($fieldName);
            if (empty($fieldValue)) {
              return $result;
            }
            $paramName = $tokenService->replace($paramName, ['form_value' => $formState->getValues()]);
          }
        }

        // Replace if need param_value.
        $paramValue = $param["param_value"];
        $tokens = $tokenService->scan($param["param_value"]);
        if (!empty($tokens['form_value'])) {
          foreach ($tokens['form_value'] as $fieldName => $token) {
            $fieldName = explode(":", $fieldName)[1];
            $fieldValue = $formState->getValue($fieldName);
            if (empty($fieldValue)) {
              return $result;
            }
            $paramValue = $tokenService->replace($paramValue, ['form_value' => $formState->getValues()]);
          }
        }

        $queryParams[$paramName] = $paramValue;
      }
    }

    if (!empty($queryParams)) {
      $requestOptions['query'] = $queryParams;
    }

    // Check endpoint.
    $endpointTokens = $tokenService->scan($endpoint);
    if (!empty($endpointTokens['form_value'])) {
      foreach ($endpointTokens['form_value'] as $fieldName => $token) {
        $fieldName = explode(":", $fieldName)[1];
        $value = $formState->getValue($fieldName);
        if (empty($value)) {
          return $result;
        }
        $endpoint = $tokenService->replace($endpoint, ['form_value' => $formState->getValues()]);
      }
    }

    /** @var \Drupal\webform_remote_fields\Service\Handler $handler */
    $handler = \Drupal::service('webform_remote_fields.handler');
    $result['data'] = $handler->get($endpoint, $requestOptions, [
      'form_state' => $formState,
      'element' => $element,
    ]);

    $event = new WebformRemoteFieldAlterResponseEvent(
      $formState,
      $element,
      $result['data'] ?? [],
    );
    \Drupal::service('event_dispatcher')
      ->dispatch($event, WebformRemoteFieldAlterResponseEvent::EVENT_NAME);

    $result['data'] = $event->getResponse();
    if (is_null($result['data'])) {
      $result['error'] = TRUE;
    }
    return $result;
  }

  /**
   * Transform #api_endpoint_params setting to array.
   *
   * @param array $element
   *   Webform element.
   *
   * @return array
   *   Array where parameter value is the key, param value it's the array value.
   */
  public static function apiEndpointParametersToArray(array $element) {
    if (empty($element['#api_endpoint_params'])) {
      return [];
    }
    $map = [];
    foreach ($element['#api_endpoint_params'] as $param) {
      $map[$param['param_name']] = $param['param_value'];
    }
    return $map;
  }

  /**
   * Extract setting of the Webform remote field element.
   *
   * The order of array with the setting values.
   *  -> api_endpoint_add_query_parameter.
   *  -> api_endpoint.
   *  -> api_result_select_text.
   *  -> headers.
   *
   * @param array $element
   *   The webform element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   An object of the form state.
   *
   * @return array
   *   Array with the key => value setting.
   */
  public static function extractElementSettings(array $element, FormStateInterface $formState) {
    $tokenService = \Drupal::token();
    $apiEndpointAddQueryParameter = $element['#api_endpoint_add_query_parameter'] ?? FALSE;
    $apiEndpoint = $element['#api_endpoint'] ?? NULL;
    $apiResultSelectValue = $element['#api_result_select_value'] ?? NULL;
    $apiResultSelectText = $element['#api_result_select_text'] ?? NULL;
    $apiNeedsHeader = $element['#needs_header'] ?? NULL;

    $custom_headers = [];
    if ($apiNeedsHeader) {
      $custom_headers = Yaml::parse($element['#headers']);
    }

    // As we have the support for get the property request value from,
    // another webform field. We have to replace the tokens for
    // #api_result_select_value setting at this point.
    if (!empty($element['#api_result_select_value'])) {
      $apiResultSelectValueToken = $tokenService->replace(
        $element['#api_result_select_value'],
        ['form_value' => $formState->getValues()],
      );
      if ($apiResultSelectValueToken) {
        $apiResultSelectValue = $apiResultSelectValueToken;
      }
    }

    return [
      'api_endpoint_add_query_parameter' => $apiEndpointAddQueryParameter,
      'api_endpoint' => $apiEndpoint,
      'api_result_select_value' => $apiResultSelectValue,
      'api_result_select_text' => $apiResultSelectText,
      'custom_headers' => $custom_headers,
      'needs_header' => $apiNeedsHeader,
      'api_round_result' => $element['#api_round_result'] ?? 0,
    ];
  }

  /**
   * Get elements id that use other fields.
   *
   * @param array $elements
   *   List of webform elements.
   *
   * @return array
   *   Array with elements id.
   */
  public static function getElementsThatUseOtherFieldsOnApi1(array $elements): array {
    $elementIds = [];
    $tokenService = \Drupal::token();

    foreach ($elements as $key => $element) {
      if (empty($element['#type'])) {
        continue;
      }

      // If the current element has children (sub-elements), recursively search
      // within them and merge results.
      $containerTypes = ['container', 'webform_wizard_page'];
      if (in_array($element['#type'], $containerTypes) && is_array($element)) {
        $elementIds = array_merge_recursive($elementIds, self::getElementsThatUseOtherFieldsOnApi1($element));
      }

      foreach ($element['#api_endpoint_params'] as $param) {
        $elementIds[$param['param_value']][] = $key;
      }

      if (!empty($element['#api_result_select_value'])) {
        $tokensInElementValue = $tokenService->scan($element['#api_result_select_value']);
        if (!empty($tokensInElementValue) && isset($tokensInElementValue['form_value'])) {
          foreach ($tokensInElementValue['form_value'] as $dependentField => $token) {
            $elementIds[$dependentField][] = $key;
          }
        }
      }

    }
    return $elementIds;
  }

  /**
   * Check if the element is triggered.
   */
  public static function isElementTriggered(array $element, FormStateInterface $formState) {
    $triggeredElement = $formState->getTriggeringElement();
    if (empty($triggeredElement)) {
      return FALSE;
    }
    return $element['#webform_key'] === $triggeredElement['#webform_key'];
  }

}
