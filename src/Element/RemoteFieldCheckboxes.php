<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Render\Element\Checkboxes;
use Drupal\webform_remote_fields\RemoteFieldOptionsProcessTrait;

/**
 * Custom form element remote checkboxes.
 *
 * Provide a way to populate checkbox options from a custom request,
 * to an external or internal service.
 *
 * @FormElement("remote_field_checkboxes")
 */
class RemoteFieldCheckboxes extends Checkboxes {

  use RemoteFieldOptionsProcessTrait;

}
