<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Render\Element\Number;
use Drupal\webform_remote_fields\RemoteFieldInputProcessTrait;

/**
 * Custom form element number.
 *
 * Provide a way to set the value getting the data from a custom request,
 * to an external or internal service.
 *
 * @FormElement("remote_field_number")
 */
class RemoteFieldNumber extends Number {

  use RemoteFieldInputProcessTrait;

}
