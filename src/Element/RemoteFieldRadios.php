<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Render\Element\Radios;
use Drupal\webform_remote_fields\RemoteFieldOptionsProcessTrait;

/**
 * Custom form element remote radios.
 *
 * Provide a way to populate radios options from a custom request,
 * to an external or internal service.
 *
 * @FormElement("remote_field_radios")
 */
class RemoteFieldRadios extends Radios {

  use RemoteFieldOptionsProcessTrait;

}
