<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\webform\Element\WebformHtmlEditor;
use Drupal\webform_remote_fields\RemoteFieldBaseTrait;

/**
 * Custom markup element to have the data populated by an external service.
 *
 * @FormElement("remote_field_markup")
 */
class RemoteFieldMarkup extends RenderElement {

  use RemoteFieldBaseTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderMarkup'],
      ],
      '#process' => [
        [$class, 'processAjax'],
      ],
    ];
  }

  /**
   * Process ajax changes.
   */
  public static function processAjax(&$element, FormStateInterface $formState, &$complete_form) {
    $apiSettings = static::extractElementSettings($element, $formState);
    $id = $element['#name'] ?? $element['#webform_key'];
    $element['#prefix'] = "<div id='{$id}-wrapper'>";
    $element['#suffix'] = "</div>";

    $response = static::processElementRequest($element, $formState);
    if (is_null($response['data'])) {
      if ($response['error']) {
        $formState->setError($element, t('An unexpected error occurred, please try later'));
      }
      return parent::processAjaxForm($element, $formState, $complete_form);
    }

    $content = $response['data'];
    if (empty($content)) {
      return $element;
    }

    /** @var \Drupal\Core\Template\TwigEnvironment $twig */
    $twig = \Drupal::service('twig');
    $template_engine = $element['#process_engine'] ?? 'replace';
    if ($template_engine === 'inline') {
      $element['#markup'] = $twig->renderInline($element['#template_string'], ['items' => $content]);
    }
    elseif ($template_engine === 'theme') {
      $render_array = [
        '#theme' => $element['#template_name'],
        '#items' => $content,
      ];
      $renderer = \Drupal::service('renderer');
      $element['#markup'] = $renderer->renderRoot($render_array);
    }
    else {
      $element['#markup'] = $content[0][$apiSettings['api_result_select_value']];
    }
    return $element;
  }

  /**
   * Create webform markup for rendering.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element.
   *
   * @return array
   *   The modified element with webform html markup.
   */
  public static function preRenderMarkup(array $element) {
    // Make sure that #markup is defined.
    if (!isset($element['#markup'])) {
      return $element;
    }

    // Replace #markup with renderable webform HTML editor markup.
    $element['markup'] = WebformHtmlEditor::checkMarkup($element['#markup'], ['tidy' => FALSE]);
    unset($element['#markup']);

    // Must set wrapper id attribute since we are no longer including #markup.
    // @see template_preprocess_form_element()
    if (isset($element['#theme_wrappers']) && !empty($element['#id'])) {
      $element['#wrapper_attributes']['id'] = $element['#id'];
    }

    // Sent #name property which is used by form-item-* classes.
    if (!isset($element['#name']) && isset($element['#webform_key'])) {
      $element['#name'] = $element['#webform_key'];
    }

    return $element;
  }

}
