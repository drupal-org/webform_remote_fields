<?php

namespace Drupal\webform_remote_fields\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Form\FormStateInterface;

/**
 * Event triggered when the response it's returned.
 *
 * Subscribing this event we have the ability to modify the response by
 * the server formatting the data to expected response format of the
 * processAjaxForm method.
 */
class WebformRemoteFieldAlterResponseEvent extends Event {

  const EVENT_NAME = 'webform_remote_fields_alter_response_element';

  /**
   * The current form state of the Webform of the element triggered the request.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $formState;

  /**
   * The element data triggered the request.
   *
   * @var array
   */
  protected array $element;

  /**
   * The response returned.
   *
   * @var array
   */
  protected array $response;

  /**
   * {@inheritDoc}
   */
  public function __construct(FormStateInterface $formState, $element, $response) {
    $this->formState = $formState;
    $this->element = $element;
    $this->response = $response;
  }

  /**
   * Returns the current form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state data.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

  /**
   * Return the element that triggered the request.
   *
   * @return array
   *   An array with element data.
   */
  public function getElement(): array {
    return $this->element;
  }

  /**
   * An array of the response data from the request.
   *
   * @return array
   *   Array with the response of the request.
   */
  public function getResponse(): array {
    return $this->response;
  }

  /**
   * Update response data to be used on the preprocessFormAjax element.
   *
   * @param array $response
   *   The new response data.
   */
  public function setResponse(array $response): void {
    $this->response = $response;
  }

}
