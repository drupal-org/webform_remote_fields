<?php

namespace Drupal\webform_remote_fields\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Webform Remote Fields settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const FORM_ID = 'webform_remote_fields_settings';

  const SETTINGS = 'webform_remote_fields.settings';

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cache_responses'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cache responses'),
      '#description' => $this->t('Check this option to cache and use cache data for all remote field requests'),
      '#default_value' => $this->config(self::SETTINGS)->get('cache_responses') ?? TRUE,
    ];

    $form['log_request_failures'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log response failure into watchdog.'),
      '#description' => $this->t('Checking this options all request failure that was made by the remote fields will stored on the watchdog.'),
      '#default_value' => $this->config(self::SETTINGS)->get('log_request_failures'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::SETTINGS)
      ->set('cache_responses', $form_state->getValue('cache_responses'))
      ->set('log_request_failures', $form_state->getValue('log_request_failures'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
