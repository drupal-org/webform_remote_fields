<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformMarkup;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'webform_markup' element.
 *
 * @WebformElement(
 *   id = "remote_field_markup",
 *   label = @Translation("Remote Field Basic HTML"),
 *   description = @Translation("Provides an element to render basic HTML markup."),
 *   category = @Translation("Remote Fields"),
 *   states_wrapper = TRUE,
 * )
 */
class WebformRemoteFieldMarkup extends WebformMarkup {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritDoc}
   */
  protected function defineDefaultProperties() {
    $custom = [
      'process_engine' => 'replace',
      'template_name' => NULL,
      'template_string' => NULL,
    ];
    return $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties()
      + $custom;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);

    $form['wr_field']['process_engine'] = [
      '#type' => 'select',
      '#title' => 'Template engine',
      '#options' => [
        'replace' => $this->t('Full replace'),
        'inline' => $this->t('Inline template'),
        'theme' => $this->t('Theme'),
      ],
      '#weight' => 4,
    ];

    $form['wr_field']['template_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of theme'),
      '#states' => [
        'visible' => [
          ':input[name="properties[process_engine]"]' => ['value' => 'theme'],
        ],
      ],
      '#weight' => 4,
    ];

    $form['wr_field']['template_string'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Type your template string.'),
      '#states' => [
        'visible' => [
          ':input[name="properties[process_engine]"]' => ['value' => 'inline'],
        ],
      ],
      '#weight' => 4,
    ];

    unset($form["markup"]);
    return $form;
  }

}
