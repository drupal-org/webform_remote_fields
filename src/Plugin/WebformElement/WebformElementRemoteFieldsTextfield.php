<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextField;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'Webform Remote Fields Textfield' element.
 *
 * @WebformElement(
 *   id = "remote_field_textfield",
 *   label = @Translation("Remote Textfield"),
 *   description = @Translation("Provides a textfield form element with API Integration."),
 *   category = @Translation("Remote Fields"),
 * )
 */
class WebformElementRemoteFieldsTextfield extends TextField {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);
    return $form;
  }

}
