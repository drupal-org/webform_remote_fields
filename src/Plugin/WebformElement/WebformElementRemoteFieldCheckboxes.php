<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Checkboxes;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'Webform Remote Fields Select' element.
 *
 * @WebformElement(
 *   id = "remote_field_checkboxes",
 *   label = @Translation("Remote Checkboxes"),
 *   description = @Translation("Provides a checkboxes form element with API Integration."),
 *   category = @Translation("Remote Fields"),
 * )
 */
class WebformElementRemoteFieldCheckboxes extends Checkboxes {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritDoc}
   */
  protected function defineDefaultProperties() {
    return ['api_result_select_text' => '']
      + $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);

    $form['wr_field']['api_result_select_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API JSON Prop - Text'),
      '#description' => $this->t('The JSON prop to be used as the select text'),
      '#weight' => 4,
      '#required' => TRUE,
    ];
    unset($form["options"]["options"]);
    unset($form["options"]["options_display_container"]);
    return $form;
  }

  /**
   * Get an element's selectors as options.
   *
   * Changing the default behavior for checkboxes from the
   * Drupal\webform\Plugin\WebformElementBase::getElementSelectorOptions
   * to add the ^ on line 78 to work with the remote checkoxes, by the default,
   * the conditions on the webform for checkxoes are create based on the
   * options create for that fields.
   * In our case these options are populated dynamically, so we need change
   * the selector to get the field name that starts with the current field name
   * instead of look for the exactly field_name + option.
   */
  public function getElementSelectorOptions(array $element) {
    if ($this->hasMultipleValues($element) && $this->hasMultipleWrapper()) {
      return [];
    }

    $pluginId = $this->getPluginId();
    $title = $this->getAdminLabel($element) . ' [' . $this->getPluginLabel() . ']';
    $name = $element['#webform_key'];

    if ($inputs = $this->getElementSelectorInputsOptions($element)) {
      $selectors = [];
      foreach ($inputs as $input_name => $input_title) {
        $multiple = ($this->hasMultipleValues($element) && $input_name === 'select') ? '[]' : '';
        $selectors[":input[name=\"{$name}[{$input_name}]$multiple\"]"] = $input_title;
      }
      return [$title => $selectors];
    }
    else {
      $multiple = ($this->hasMultipleValues($element) && strpos($pluginId, 'select') !== FALSE) ? '[]' : '';
      // Here is the unique change from the base method.
      // Adding the ^ on the selector field.
      return [":input[name^=\"$name$multiple\"]" => $title];
    }
  }

}
