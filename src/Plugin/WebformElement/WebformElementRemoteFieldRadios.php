<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Radios;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'Webform Remote Fields Select' element.
 *
 * @WebformElement(
 *   id = "remote_field_radios",
 *   label = @Translation("Remote Radios"),
 *   description = @Translation("Provides a radios form element with API Integration."),
 *   category = @Translation("Remote Fields"),
 * )
 */
class WebformElementRemoteFieldRadios extends Radios {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritDoc}
   */
  protected function defineDefaultProperties() {
    return ['api_result_select_text' => '']
      + $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);

    $form['wr_field']['api_result_select_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API JSON Prop - Text'),
      '#description' => $this->t('The JSON prop to be used as the select text'),
      '#weight' => 4,
      '#required' => TRUE,
    ];
    unset($form["options"]["options"]);
    unset($form["options"]["options_display_container"]);
    return $form;
  }

}
