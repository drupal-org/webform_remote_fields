<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Number;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'Webform Remote Fields Number' element.
 *
 * @WebformElement(
 *   id = "remote_field_number",
 *   label = @Translation("Remote Number"),
 *   description = @Translation("Provides a number form element with API Integration."),
 *   category = @Translation("Remote Fields"),
 * )
 */
class WebformRemoteFieldsNumber extends Number {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties()
      + ['api_round_result' => 0];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);
    $form['wr_field']['api_round_result'] = [
      '#type' => 'number',
      '#title' => $this->t('Round result'),
      '#description' => $this->t('Specify the number of decimal places for the calculation result.'),
      '#weight' => 4,
    ];
    return $form;
  }

}
