<?php

namespace Drupal\webform_remote_fields\Service;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Helper class to provide feature about remote field elements.
 */
class WebformRemoteFieldHelper {

  /**
   * Plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
   *   The plugin manager of the custom elements.
   */
  public function __construct(PluginManagerInterface $pluginManager) {
    $this->pluginManager = $pluginManager;
  }

  /**
   * Get all custom remote field elements.
   *
   * @return array
   *   An array with elements.
   */
  public function getElements() {
    $ourPlugins = array_filter($this->pluginManager->getDefinitions(), function ($definition) {
      return isset($definition['provider']) && $definition['provider'] === 'webform_remote_fields';
    });
    return $ourPlugins;
  }

}
