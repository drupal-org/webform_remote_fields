<?php

namespace Drupal\webform_remote_fields\Service;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Custom middleware that overrides the requests behavior.
 *
 * We use this middleware to do the request in order for us be able to
 * modify the request or responses before used it on the form elements.
 */
class WebformRemoteFieldsClientMiddleware {

  /**
   * Implements __invoke() function.
   */
  public function __invoke(callable $handler) {
    return function (
      RequestInterface $request,
      array $options
    ) use ($handler) {
      // Modify the request object or options here if needed.
      // @codingStandardsIgnoreStart
      return $handler($request, $options)->then(
        function (ResponseInterface $response) use ($request) {
          // Modify the response object here if needed.
          return $response;
        }
      );
      // @codingStandardsIgnoreEnd
    };
  }

}
