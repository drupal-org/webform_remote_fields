<?php

namespace Drupal\webform_remote_fields\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\webform_remote_fields\Event\WebformRemoteFieldsPreprocessRequest;
use Drupal\webform_remote_fields\Form\SettingsForm;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Handler webfor remote fields api requests.
 */
class Handler {

  const CACHE_TAG = 'webform_remote_fields_apis';

  /**
   * The webform_remote_fields.http_client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Cache service to store api responses.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Config settings of the module.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected Config $config;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected RequestStack $request;

  /**
   * Time intertace service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Constructs a Handler object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The webform_remote_fields.http_client service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache service.
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *   Config service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request data.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service interface.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger, CacheBackendInterface $cache, ConfigFactoryInterface $config, EventDispatcherInterface $eventDispatcher, RequestStack $request, TimeInterface $time) {
    $this->httpClient = $http_client;
    $this->logger = $logger->get('webform_remote_fields');
    $this->cache = $cache;
    $this->config = $config->get(SettingsForm::SETTINGS);
    $this->eventDispatcher = $eventDispatcher;
    $this->request = $request;
    $this->time = $time;
  }

  /**
   * Update the endpoint with the full information for internal api.
   *
   * @param string $endpoint
   *   The endpoint to reach.
   *
   * @return string
   *   The converted endpoint for internal, Otherwise the endpoint as received.
   */
  protected function prepareEndpointUrl(string $endpoint) {
    if (str_starts_with($endpoint, '[internal]')) {
      $internalServer = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
      return str_replace('[internal]', $internalServer, $endpoint);
    }
    return $endpoint;
  }

  /**
   * Helper function to generate cached id based on the request settings.
   *
   * @param string $uri
   *   The endpoint url.
   * @param array $options
   *   Array of request options.
   *
   * @return string
   *   The cid with $uri + options as mapped string.
   */
  public static function generateCid($uri, $options) {
    $cid = $uri;
    if (!empty($options['query'])) {
      $params = [];
      $cid .= ':';
      foreach ($options['query'] as $param => $value) {
        $params[] = "{$param}_{$value}";
      }
      $cid .= implode(':', $params);
    }
    return $cid;
  }

  /**
   * Get the data from the api or cache.
   */
  public function get(string $uri, array $options, $webformInfo) {
    $event = new WebformRemoteFieldsPreprocessRequest(
      $webformInfo['form_state'],
      $webformInfo['element'],
      $uri,
      $options,
    );

    $this->eventDispatcher
      ->dispatch($event, WebformRemoteFieldsPreprocessRequest::EVENT_NAME);

    $uri = $event->getUri();
    $options = $event->getOptions();

    $isCached = $this->shouldCacheResponse($uri, $options);
    if ($isCached) {
      return $isCached->data;
    }
    $response = $this->getFromApi($uri, $options);
    if (!empty($response)) {
      $this->cacheApiResponse($response, $uri, $options);
    }
    return $response;
  }

  /**
   * Cache the api response settings 24 ours of expiration time.
   *
   * Also set tags like:
   *  -> the endpoint itself
   *  -> the endpoint itself + the cid generated.
   *
   * @param array $response
   *   The response data.
   * @param string $uri
   *   The uri.
   * @param array $options
   *   The options of the request.
   */
  protected function cacheApiResponse($response, $uri, $options) {
    if (!$this->config->get('cache_responses')) {
      return NULL;
    }
    $cid = self::generateCid($uri, $options);
    $this->cache->set(
      self::generateCid($uri, $options),
      $response,
      $this->time->getRequestTime() + 86400,
      [
        self::CACHE_TAG,
        self::CACHE_TAG . ':' . $uri,
        self::CACHE_TAG . ':' . $cid,
      ]
    );
  }

  /**
   * Get data from the cache.
   */
  protected function shouldCacheResponse($uri, $options) {
    if (!$this->config->get('cache_responses')) {
      return NULL;
    }
    return $this->cache->get(self::generateCid($uri, $options));
  }

  /**
   * Make a request using webform_remote_fields http client.
   *
   * @param string $uri
   *   The endpoint of the api.
   * @param array $options
   *   The request options.
   *
   * @return array|null
   *   Returns array with the response. Null for any kind of error.
   */
  protected function getFromApi(string $uri, array $options) {
    try {
      $endpoint = $this->prepareEndpointUrl($uri);
      $response = $this->httpClient
        ->get($endpoint, $options);
      if ($response->getStatusCode() !== 200) {
        $this->logRequestFailures(
            "Response from @endpoint returned status code @code using options @options.",
            [
              '@endpoint' => $uri,
              '@code' => $response->getStatusCode(),
              '@options' => json_encode($options),
            ]
          );
        return NULL;
      }
      $json_response = json_decode($response->getBody()->getContents(), TRUE);
      if (!is_array($json_response)) {
        return NULL;
      }
      return $json_response;
    }
    catch (GuzzleException $exception) {
      $this->logRequestFailures(
        "Failed to get data from @endpoint using options @options. Error: @errorMessage",
        [
          '@endpoint' => $uri,
          '@options' => json_encode($options),
          '@errorMessage' => $exception->getMessage(),
        ]
      );
      return NULL;
    }
    catch (\Exception $exception) {
      $this->logRequestFailures(
        "Failed to get data from @endpoint using options @options. Error: @errorMessage",
        [
          '@endpoint' => $uri,
          '@options' => json_encode($options),
          '@errorMessage' => $exception->getMessage(),
        ]
      );
      return NULL;
    }
  }

  /**
   * Log requests failures if the log is enabled.
   *
   * @param string $message
   *   The message to be looged.
   * @param array $params
   *   The parameters of the message if needed.
   */
  private function logRequestFailures(string $message, array $params = []) {
    if (!$this->config->get('log_request_failures')) {
      return NULL;
    }
    $this->logger->error($message, $params);
  }

}
