<?php

namespace Drupal\webform_remote_fields_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Webform Remote Fields Examples routes.
 */
class ShoesAPIExample extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Request $request) {
    $category = $request->get('category');
    $brands = $request->get('brands');
    return new JsonResponse([
      ['label' => 'Product of category ' . $category . ' and brand ' . $brands],
    ]);
  }

}
