<?php

namespace Drupal\webform_remote_fields_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Webform Remote Fields Examples routes.
 */
class WebformRemoteFieldsExamplesFakeAPI extends ControllerBase {

  /**
   * Builds the response.
   */
  public function listProducts() {
    $products = [];
    for ($i = 1; $i < 11; $i++) {
      $products[] = [
        'id' => $i,
        'value' => 'Product ID ' . $i,
      ];
    }
    return new JsonResponse($products);
  }

  /**
   * Fake api example.
   */
  public function getProductPrice(Request $request) {
    $prices = [];
    for ($i = 1; $i < 11; $i++) {
      $prices[$i] = rand(0.5, 100);
    }
    $value = $prices[$request->get('product_id')];
    return new JsonResponse([
      ['value' => $value],
    ]);
  }

}
