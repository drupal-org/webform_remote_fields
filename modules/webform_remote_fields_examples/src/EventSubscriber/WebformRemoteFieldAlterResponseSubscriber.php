<?php

namespace Drupal\webform_remote_fields_examples\EventSubscriber;

use Drupal\webform_remote_fields\Event\WebformRemoteFieldAlterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * WebformRemoteFieldAlterResponse example.
 */
class WebformRemoteFieldAlterResponseSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      WebformRemoteFieldAlterResponseEvent::EVENT_NAME => [
        ['onWebformRemoteFieldAlterResponse', 0],
      ],
    ];
  }

  /**
   * Implement the alter response example.
   */
  public function onWebformRemoteFieldAlterResponse(WebformRemoteFieldAlterResponseEvent $remoteFieldAlterResponseEvent) {
    // Do whatever you want you need to the response.
  }

}
