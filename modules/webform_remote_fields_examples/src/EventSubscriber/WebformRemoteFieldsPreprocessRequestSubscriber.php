<?php

namespace Drupal\webform_remote_fields_examples\EventSubscriber;

use Drupal\webform_remote_fields\Event\WebformRemoteFieldsPreprocessRequest;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * WebformRemoteFieldsPreprocessRequest example.
 */
class WebformRemoteFieldsPreprocessRequestSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      WebformRemoteFieldsPreprocessRequest::EVENT_NAME => [
        ['onWebformRemoteFieldsPreprocessRequest', 0],
      ],
    ];
  }

  /**
   * Alter preprocess request example.
   */
  public function onWebformRemoteFieldsPreprocessRequest(WebformRemoteFieldsPreprocessRequest $webformRemoteFieldsPreprocessRequest) {
    // @codingStandardsIgnoreStart
    $uri = $webformRemoteFieldsPreprocessRequest->getUri();
    $options = $webformRemoteFieldsPreprocessRequest->getOptions();
    $formState = $webformRemoteFieldsPreprocessRequest->getFormState();
    $element = $webformRemoteFieldsPreprocessRequest->getElement();
    // @codingStandardsIgnoreEnd
  }

}
