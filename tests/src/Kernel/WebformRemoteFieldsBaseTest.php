<?php

namespace Drupal\Tests\webform_remote_fields\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_remote_fields\RemoteFieldBaseTrait;

/**
 * Test remote field element setting submit.
 */
class WebformRemoteFieldsBaseTest extends KernelTestBase {

  /**
   * Modules to be enabled.
   *
   * @var string[]
   */
  protected static $modules = ['webform_remote_fields'];

  /**
   * Test dependency values.
   *
   * @dataProvider dataProviderGetFieldDependenciesValues
   */
  public function testGetFieldDependenciesValues($element, $values, $expected_array, $incomplete) {
    [
      $api_dependencies_complete,
      $computedValues,
    ] = RemoteFieldBaseTrait::getFieldDependenciesValuesFromElement($element, $values);
    $this->assertEquals($expected_array, $computedValues);
    $this->assertEquals($api_dependencies_complete, $incomplete);
  }

  /**
   * Data provider for testGetFieldDependenciesValues.
   *
   * @return \Generator
   *   All data to test the scenarios.
   */
  public function dataProviderGetFieldDependenciesValues() {
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
        ],
      ],
      ['field_x' => 1],
      ['param_1' => 1],
      TRUE,
    ];
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
        ],
      ],
      ['field_x' => NULL],
      [],
      FALSE,
    ];
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
          ['param_name' => 'param_2', 'param_value' => 'field_y'],
        ],
      ],
      ['field_x' => 1, 'field_y' => 2],
      ['param_1' => 1, 'param_2' => 2],
      TRUE,
    ];
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
          ['param_name' => 'param_2', 'param_value' => 'field_y'],
        ],
      ],
      ['field_x' => 1],
      ['param_1' => 1],
      FALSE,
    ];
  }

}
