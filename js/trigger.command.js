/**
 * Custom ajax command to trigger element value changed,
 * as we update the value/options of the elements using ajax,
 * we need to support that libraries can listener this value changes.
 */
(function (Drupal) {
  Drupal.AjaxCommands.prototype.webformRemoteFieldTriggerChangeValue = function (ajax, response, status, args) {
    const form = ajax.$form[0];
    const element = form.querySelector(response.selector);
    if (element) {
      let event = new Event('change', { 'bubbles': true, 'cancelable': true });
      element.dispatchEvent(event);
    }
  };
})(Drupal);


