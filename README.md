# Webform Remote Fields

## Module Summary

The Webform Remote Fields module is an extension of the Webform module for Drupal. It provides seven new Webform element types that allow you to interact with remote data sources, making your webforms dynamic and responsive to user actions. These remote fields can populate form fields with data obtained from external REST services. Additionally, the module offers the capability to configure field dependencies, ensuring that specific fields respond to changes in other fields.

## What the Module Does

This module enhances the functionality of webforms by introducing seven new field types:

1. **Remote Checkboxes**: Allows users to select multiple options from a dynamic list populated by a remote data source.

2. **Remote Field Basic HTML**: Provides a basic HTML field that can be dynamically filled with content retrieved from an external service.

3. **Remote Hidden**: A hidden field that can be populated with data from an external source, useful for storing additional information.

4. **Remote Number**: A numeric field that can be automatically updated with values from a remote service.

5. **Remote Radios**: Similar to remote checkboxes but allows users to select only one option from a dynamic list.

6. **Remote Select**: Provides a select dropdown menu with options fetched from an external source.

7. **Remote Textfield**: A textfield that can be filled with dynamic data retrieved from an external service.

## Configuring a Remote Field

### 1. Adding a Remote Field

To create a remote field, follow these steps:

1. Go to the Webform you're working on.
2. Add a new field to the form.
3. Select the desired remote field type (e.g., Remote Select, Remote Textfield).
4. Configure the basic field settings such as label, description, and machine name.

### 2. Setting the API Endpoint

In the field settings, locate the "API Endpoint" option. Here, you need to provide the API endpoint URL that the field will use to make requests and retrieve data. This URL is the address of the external service that provides the data you want to populate the field with.

### 3. Mapping Data Properties

You will also find options to map the data properties received from the API response to the field:

- **API JSON Prop - Value**: Specify the JSON property that represents the value for each option.
- **API JSON Prop - Text**: Specify the JSON property that represents the text or label for each option.

### Configuring Dependency

If you want a remote field to depend on another field, follow these steps:

1. In the field settings, check the "Add query parameters" option.
2. In the "The API endpoint param name" field, specify the query parameter that the API endpoint expects to filter the data.
3. In the "The API endpoint param value" field, enter the name of the field in the Webform that provides the value to be used as a parameter in the API request.

This configuration allows the remote field to respond to changes in the dependent field, ensuring that the data it retrieves from the API remains relevant to the user's selections.

### The remote fields supports the existing Webform field features like

- Conditions
- Empty option / label for select
- Include 'All of the above' option and Include 'None of the above' option for checkboxes
- Element Description/Help/More
- For Remote Select field you can set the Select2 style
- Form Validation
- All other features was not tested yet, but maybe can also work as well.

### How to use / configure remote fields.

1. API endpoint: Add the url of the service. For interal url add the prefix [internal]/followed-by-uri
2. Add custom headers: Sometimes we need to configure api-key, authorization or any kind of header to be able to reach the service.
   For this case we can use this option to add custom header to the request. For example. <br>
   Check the option - Add Custom headers and a textarea will open to you configure the headers like:<br>
   Authorization: Basic my-token-here<br>
   or<br>
   api-key: my-api-key<br>
   As you noticed in examples above, to configure the header you must separate the header name with ":". So, the configuration became Header-Name: header-value<br>
3. API JSON Prop - Value: Inform the name of the property on the response array to used as value.<br>
   For select, checkboxes and radio this config will be used to set the option value<br>
   For the other fields will be value to populate the input.
4. API JSON Prop - Text: This configuration applies only for select, radios and checkboxes. This is the property name of the result to used as label on the fields mentioned.
5. For both options above, API JSON Prop - Value/Text we can also use tokens. If we want to use any existing field value on the Webform to make the request, just set which field name to use as value or label.<br>
   For example: [form_value:name:field_name].
6. API JSON Prop - Value: This option create query params for the request dynamically by using the proper form values, let's suppose you want to get list of options but this options depends a filter. For example.<br>
   Let's use the service https://jsonplaceholder.typicode.com/ as example.<br>
   Let's suppose you want to filter the albums by user, and you have a select that list these users. https://jsonplaceholder.typicode.com/users <br>
   You can create any kind of remote field and check the **Add query parameters** option.
   Then, you need to set The API endpoint param name(the query parameter name) as userId.
   Finally, you just need to se which field on the form will be used to provide the value for the request and set the value for the parameter. In our case for example will be the name of field select user list.<br>
   For any doubts, please install the webform_remote_fields_examples to get some Webforms created to check the configuration.<br>
   Last but no least, you can configure more than one query parameter.

### Caching
All request are cached by default, althose can be disabled accessing _/admin/config/system/settings_.<br>
The response are kept in cache for 24hours.<br>
A custom cache bin(webform_remote_fields) were defined to this cache data
The following tags are create to the cache.
- webform_remote_fields
- webform_remote_fields_apis:$uri
  -  Where the uri is the endpoint configured in the field.
- webform_remote_fields_apis:$cid
  -  Where cid is the $uri + all query parameters separeted by : followed by the query param name _ query param value.
  - Any doubts take a loot at Drupal\webform_remote_fields\Service::generateCID

### Response status code
Any different status code of `200` is considered **failed**.<br>
All details about failed response will be logged on the watchdog if the setting(_Log response failure into watchdog._) is enabled. To control this flag you can configure on `/admin/config/system/settings`, by default the log aren't stored.

### Response expected data format
We are considering an array of data on all responses like:<br>
```php
[
  0 => ['prop_name' => 'prop_value', 'other_prop_name' => 'other_prop_value'],
  1 => ['prop_name' => 'prop_value', 'other_prop_name' => 'other_prop_value'],
];
```
For the Select, Checkboxes and Radios we are going to iterate by the response as normal.<br>
For Markup and the other fields we are going to get the first value of the array.<br>
This is the default behavior.<br>

**_How to extend that if the service can't return the data as expected format above?_**<br>
We are triggering a custom event called **_WebformRemoteFieldResponseElement_** that you have access to the `$form_state`, `$element` and the `response` itself.
You are able to implement a custom listener to this event and change whatever you need to attend the module expected response.

### Events

1. **WebformRemoteFieldAlterResponseEvent**: Subscribing this event you are able to change the response data before it be used by the processAjaxForm elements method.<br>
   Rest services or any kind of service has different ways to return the data. If your service do not return the data on the expected format, you can subscribe this event to modify this data before be used by the element.<br>
   This event has the following data:<br>
   `$form_state` of the current Webform and form data.<br>
   `$element` that triggered the request.<br>
   `$response` the value returned by the service, here is the data that you can modify just using the $event->setResponse() on the event subscribe them the value will be sent to the processAjaxForm
2. **WebformRemoteFieldsPreprocessRequest**: Subscribing this event you are able to modify the data used to make the request. If your rest service expects the query parameter or headers in a different way the modules sent the data. You can modify the request options before be sent to adapt with your rest service.<br>
   This event provide you the following data.<br>
   `$form_state` of the current Webform and form data.<br>
   `$element` that triggered the request.<br>
   `$uri` the endpoint url to be reached. If you want to update this value before do the request you can update by `$event->setUri('new value');`<br>
   `$options` used on the client http to compose the request. If you want to update this value you can also `$event->setOptions();`<br>

### Markup remote fields

Some considerations regarding this element.<br>
This element sounds unhelpful, but sometimes we may want to display a text or any kind of markup returned by the service.<br>
At this moment the markup returned are not stored(as we get the value from another instance). That means if you want to use the submission on the PDF or any kind of way it's not possible. But we already tracked this feature request and probably in the next version will be possible to use.<br>
Finally, this field has 3 ways to render the data.
1. Full replace. The entire response by the service will be rendered on the Webform.
2. Inline template, here you have the ability to write a custom inline twig template to process the response of the service.<br>
   As we process this data as twig theme, all twig functions are able to be used.<br>
   The variable items is available on the twig to used as an array of each item returned by the service.
   Finally, to use this option, who are working on the Webform must have the permission `administer webform_remote_fields template configuration`<br>
   As this option is more advanced than others, make sure to set this permission for the correct users that knows how to use/explore twig features.
3. Theme, here is an option that a Drupal theme will be used to render the response, can be your custom theme or any theme defined by Drupal or contrib module.<br>
   This option also needs the `administer webform_remote_fields template configuration` permission.

---
With the Webform Remote Fields module, you can create dynamic webforms that fetch and display real-time data from remote sources. The module's configuration options make it easy to set up these remote fields and establish dependencies, enhancing the user experience and data collection capabilities of your webforms.
